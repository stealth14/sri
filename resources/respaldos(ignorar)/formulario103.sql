drop table formulario103
create table formulario103(
	id int identity(1,1),
	/*la magia*/
	userid nvarchar (450),

	/*la magia*/
	c101 int ,
	c102 int ,
	c104 int ,
	c201 int ,
	c202 int ,
	/*magia*/
	c302 int ,
	c352 int ,
	/*magia*/
	c303 int ,
	c353 int ,
	/*magia*/
	c304 int ,
	c354 int ,
	/*magia*/
	c307 int ,
	c357 int ,
	/*magia*/
	c308 int ,
	c358 int ,
	/*magia*/
	c309 int ,
	c359 int ,
	/*magia*/
	c310 int ,
	c360 int ,
	/*magia*/
	c311 int ,
	c361 int ,
	/*magia*/
	c312 int ,
	c362 int ,
	/*magia*/
	c314 int ,
	c364 int ,
	/*magia*/
	c319 int ,
	c369 int ,
	/*magia*/
	c320 int ,
	c370 int ,
	/*magia*/
	c322 int ,
	c372 int ,
	/*magia*/
	c323 int ,
	c373 int ,
	/*magia*/
	c324 int ,
	c374 int ,
	/*magia*/
	c325 int ,
	c375 int ,
	/*magia*/
	c326 int ,
	c376 int ,
	/*magia*/
	c327 int ,
	c377 int ,
	/*magia*/
	c328 int ,
	c378 int ,

	/*magia*/
	c329 int ,
	c379 int ,
	/*magia*/
	c330 int ,
	c380 int ,
	/*magia*/
	c331 int ,
	/*magia*/
	c332 int ,
	/*magia*/
	c333 int ,
	c383 int ,

	/*magia*/
	c334 int ,
	c384 int ,

	/*magia*/
	c335 int ,
	c385 int ,

	/*magia*/
	c336 int ,
	c386 int ,

	/*campos especiales*/
	c510 int ,
	c520 int ,
	c530 int ,
	c540 int ,
	c550 int ,
	/*campos especiales*/

	/*magia*/
	c337 int ,
	c387 int ,

	/*magia*/
	c338 int ,
	c388 int ,

	/*magia*/
	c339 int ,
	c389 int ,

	/*magia*/
	c340 int ,
	c390 int ,

	/*magia*/
	c341 int ,
	c391 int ,

	/*magia*/
	c342 int ,
	c392 int ,

	/*magia*/
	c343 int ,
	c393 int ,

	/*magia*/
	c344 int ,
	c394 int ,

	/*magia*/
	c345 int ,
	c395 int ,

	/*magia*/
	c346 int ,
	c396 int ,

	/*magia*/
	c349 int ,
	c399 int ,

	/*magia*/
	c402 int ,
	c452 int ,

		/*magia*/
	c403 int ,
	c453 int ,

		/*magia*/
	c404 int ,
	c454 int ,

		/*magia*/
	c405 int ,

	/*magia*/
	c406 int ,
	c456 int ,

			/*magia*/
	c407 int ,
	c457 int ,

			/*magia*/
	c408 int ,
	c458 int ,

			/*magia*/
	c409 int ,
	c459 int ,

			/*magia*/
	c410 int ,
	c460 int ,

			/*magia*/
	c411 int ,
	c461 int ,


			/*magia*/
	c412 int ,

			/*magia*/
	c413 int ,
	c463 int ,

			/*magia*/
	c414 int ,
	c464 int ,

			/*magia*/
	c415 int ,
	c465 int ,

			/*magia*/
	c416 int ,

	/*magia*/
	c417 int ,
	c467 int ,

		/*magia*/
	c418 int ,
	c468 int ,

		/*magia*/
	c419 int ,
	c469 int ,

		/*magia*/
	c420 int ,
	c470 int ,

		/*magia*/
	c421 int ,
	c471 int ,

		/*magia*/
	c422 int ,
	c472 int ,

		/*magia*/
	c423 int ,

		/*magia*/
	c424 int ,
	c474 int ,


	/*magia*/
	c425 int ,
	c475 int ,

		/*magia*/
	c426 int ,
	c476 int ,

			/*magia*/
	c427 int ,
	c477 int ,

			/*magia*/
	c428 int ,
	c478 int ,

			/*magia*/
	c429 int ,
	c479 int ,

			/*magia*/
	c430 int ,
	c480 int ,

			/*magia*/
	c431 int ,
	c481 int ,

			/*magia*/
	c432 int ,
	c482 int ,

			/*magia*/
	c433 int ,

			/*magia*/
	c497 int ,
	c498 int ,

	/*total*/
	c499 int ,
		
	/*footer*/
	c890 int ,

	c897 int ,
	c898 int ,
	c899 int ,
	c880 int ,
	
	c902 int ,

	c903 int ,

	c904 int ,

	c999 int ,

	c905 int ,

	c907 int ,

	c908 int ,

	c909 int ,

	c910 int ,

	c911 int ,

	c812 int ,

	c813 int ,

	c815 int ,

	c198 int ,

	c199 int ,

	)

drop table auditoria
create table auditoria
( 
	operacion varchar(20),
	formulario varchar(20),
	fecha date,
	userid nvarchar(450),
	usermail nvarchar(100)
)

/*TRIGGERS*/
--auditoria


select * from auditoria
/*
agregar atributo mail a los formularios
reensamblar clases mvc de formulario
agregar en el trigger
*/

/*auditoria*/

create trigger form103_audit on dbo.formulario103
for insert,update,delete 
as

DECLARE @operacion varchar(20)
IF EXISTS(SELECT * FROM inserted)
  IF EXISTS(SELECT * FROM deleted)
    set @operacion = 'update'
ELSE
    SELECT @operacion = 'insert'
ELSE
  IF EXISTS(SELECT * FROM deleted)
    set @operacion = 'delete'
  ELSE
    --no rows affected - cannot determine event
    set @operacion = 'unknown'

/*condicionales*/

	declare @formulario varchar(20);
	declare @fecha date;
	declare @userid nvarchar(450);
	declare @usermail nvarchar(100);

	set @formulario='103';
	set @fecha=GETDATE();
	set @userid = (select userid from inserted);
	set @usermail = 'probando'

	insert into auditoria values(@operacion,@formulario,@fecha,@userid,@usermail);

go

select * from auditoria

/*validaciones en bd*/

create trigger  on dbo.