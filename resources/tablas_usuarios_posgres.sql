--
-- PostgreSQL database dump
--

-- Dumped from database version 10.10
-- Dumped by pg_dump version 10.10

-- Started on 2020-02-11 15:03:57

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12924)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2864 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 198 (class 1259 OID 16483)
-- Name: AspNetRoleClaims; Type: TABLE; Schema: public; Owner: ronny
--

CREATE TABLE public."AspNetRoleClaims" (
    "Id" integer NOT NULL,
    "RoleId" text NOT NULL,
    "ClaimType" text,
    "ClaimValue" text
);


ALTER TABLE public."AspNetRoleClaims" OWNER TO ronny;

--
-- TOC entry 197 (class 1259 OID 16467)
-- Name: AspNetRoles; Type: TABLE; Schema: public; Owner: ronny
--

CREATE TABLE public."AspNetRoles" (
    "Id" text NOT NULL,
    "Name" text,
    "NormalizedName" text,
    "ConcurrencyStamp" text
);


ALTER TABLE public."AspNetRoles" OWNER TO ronny;

--
-- TOC entry 199 (class 1259 OID 16496)
-- Name: AspNetUserClaims; Type: TABLE; Schema: public; Owner: ronny
--

CREATE TABLE public."AspNetUserClaims" (
    "Id" integer NOT NULL,
    "UserId" text NOT NULL,
    "ClaimType" text,
    "ClaimValue" text
);


ALTER TABLE public."AspNetUserClaims" OWNER TO ronny;

--
-- TOC entry 200 (class 1259 OID 16509)
-- Name: AspNetUserLogins; Type: TABLE; Schema: public; Owner: ronny
--

CREATE TABLE public."AspNetUserLogins" (
    "LoginProvider" text NOT NULL,
    "ProviderKey" text NOT NULL,
    "ProviderDisplayName" text,
    "UserId" text NOT NULL
);


ALTER TABLE public."AspNetUserLogins" OWNER TO ronny;

--
-- TOC entry 201 (class 1259 OID 16522)
-- Name: AspNetUserRoles; Type: TABLE; Schema: public; Owner: ronny
--

CREATE TABLE public."AspNetUserRoles" (
    "UserId" text NOT NULL,
    "RoleId" text NOT NULL
);


ALTER TABLE public."AspNetUserRoles" OWNER TO ronny;

--
-- TOC entry 202 (class 1259 OID 16540)
-- Name: AspNetUserTokens; Type: TABLE; Schema: public; Owner: ronny
--

CREATE TABLE public."AspNetUserTokens" (
    "UserId" text NOT NULL,
    "LoginProvider" text NOT NULL,
    "Name" text NOT NULL,
    "Value" text
);


ALTER TABLE public."AspNetUserTokens" OWNER TO ronny;

--
-- TOC entry 203 (class 1259 OID 16570)
-- Name: AspNetUsers; Type: TABLE; Schema: public; Owner: ronny
--

CREATE TABLE public."AspNetUsers" (
    "Id" text NOT NULL,
    "UserName" text,
    "NormalizedUserName" text,
    "Email" text,
    "NormalizedEmail" text,
    "EmailConfirmed" boolean NOT NULL,
    "PasswordHash" text,
    "SecurityStamp" text,
    "ConcurrencyStamp" text,
    "PhoneNumber" text,
    "PhoneNumberConfirmed" boolean NOT NULL,
    "TwoFactorEnabled" boolean NOT NULL,
    "LockoutEnd" text,
    "LockoutEnabled" boolean NOT NULL,
    "AccessFailedCount" integer NOT NULL
);


ALTER TABLE public."AspNetUsers" OWNER TO ronny;

--
-- TOC entry 196 (class 1259 OID 16462)
-- Name: __EFMigrationsHistory; Type: TABLE; Schema: public; Owner: ronny
--

CREATE TABLE public."__EFMigrationsHistory" (
    "MigrationId" character varying(150) NOT NULL,
    "ProductVersion" character varying(32) NOT NULL
);


ALTER TABLE public."__EFMigrationsHistory" OWNER TO ronny;

--
-- TOC entry 2851 (class 0 OID 16483)
-- Dependencies: 198
-- Data for Name: AspNetRoleClaims; Type: TABLE DATA; Schema: public; Owner: ronny
--

COPY public."AspNetRoleClaims" ("Id", "RoleId", "ClaimType", "ClaimValue") FROM stdin;
\.


--
-- TOC entry 2850 (class 0 OID 16467)
-- Dependencies: 197
-- Data for Name: AspNetRoles; Type: TABLE DATA; Schema: public; Owner: ronny
--

COPY public."AspNetRoles" ("Id", "Name", "NormalizedName", "ConcurrencyStamp") FROM stdin;
\.


--
-- TOC entry 2852 (class 0 OID 16496)
-- Dependencies: 199
-- Data for Name: AspNetUserClaims; Type: TABLE DATA; Schema: public; Owner: ronny
--

COPY public."AspNetUserClaims" ("Id", "UserId", "ClaimType", "ClaimValue") FROM stdin;
\.


--
-- TOC entry 2853 (class 0 OID 16509)
-- Dependencies: 200
-- Data for Name: AspNetUserLogins; Type: TABLE DATA; Schema: public; Owner: ronny
--

COPY public."AspNetUserLogins" ("LoginProvider", "ProviderKey", "ProviderDisplayName", "UserId") FROM stdin;
\.


--
-- TOC entry 2854 (class 0 OID 16522)
-- Dependencies: 201
-- Data for Name: AspNetUserRoles; Type: TABLE DATA; Schema: public; Owner: ronny
--

COPY public."AspNetUserRoles" ("UserId", "RoleId") FROM stdin;
\.


--
-- TOC entry 2855 (class 0 OID 16540)
-- Dependencies: 202
-- Data for Name: AspNetUserTokens; Type: TABLE DATA; Schema: public; Owner: ronny
--

COPY public."AspNetUserTokens" ("UserId", "LoginProvider", "Name", "Value") FROM stdin;
\.


--
-- TOC entry 2856 (class 0 OID 16570)
-- Dependencies: 203
-- Data for Name: AspNetUsers; Type: TABLE DATA; Schema: public; Owner: ronny
--

COPY public."AspNetUsers" ("Id", "UserName", "NormalizedUserName", "Email", "NormalizedEmail", "EmailConfirmed", "PasswordHash", "SecurityStamp", "ConcurrencyStamp", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEnd", "LockoutEnabled", "AccessFailedCount") FROM stdin;
e3929060-0542-49b3-8b57-3b0eecd2d4b8	ronny1418@hotmail.com	RONNY1418@HOTMAIL.COM	ronny1418@hotmail.com	RONNY1418@HOTMAIL.COM	f	AQAAAAEAACcQAAAAEGAbYgfnb+IV/ElFpE3Abl2xp96pqsRGlH2BwSlTrjTi421zG6dFdfkUWMsFlGwHbw==	KSMQMHUEYASNPCAIAOPNLEH6U5RYDGYB	0f2f30f4-1b7c-47af-b601-69a8f27b6af9	\N	f	f	\N	t	0
36a6e897-fb01-4ec2-b2d0-c22eb3be02c0	pato1418@yahoo.com	PATO1418@YAHOO.COM	pato1418@yahoo.com	PATO1418@YAHOO.COM	t	AQAAAAEAACcQAAAAEMtnJ3WyqjMZR1mLmXlzGA6Poyen32dnRGyQPqRj85J7mRs7gWJDK6kMFEgdQ5Y4SA==	HC6FGTMBOVIM3QNRZCDWQ6M2BQLN52UR	fcd17ef6-ec0a-4f7a-87f9-977bdd97dfce	\N	f	f	\N	t	0
\.


--
-- TOC entry 2849 (class 0 OID 16462)
-- Dependencies: 196
-- Data for Name: __EFMigrationsHistory; Type: TABLE DATA; Schema: public; Owner: ronny
--

COPY public."__EFMigrationsHistory" ("MigrationId", "ProductVersion") FROM stdin;
00000000000000_CreateIdentitySchema	3.1.1
\.


--
-- TOC entry 2710 (class 2606 OID 16490)
-- Name: AspNetRoleClaims PK_AspNetRoleClaims; Type: CONSTRAINT; Schema: public; Owner: ronny
--

ALTER TABLE ONLY public."AspNetRoleClaims"
    ADD CONSTRAINT "PK_AspNetRoleClaims" PRIMARY KEY ("Id");


--
-- TOC entry 2706 (class 2606 OID 16474)
-- Name: AspNetRoles PK_AspNetRoles; Type: CONSTRAINT; Schema: public; Owner: ronny
--

ALTER TABLE ONLY public."AspNetRoles"
    ADD CONSTRAINT "PK_AspNetRoles" PRIMARY KEY ("Id");


--
-- TOC entry 2713 (class 2606 OID 16503)
-- Name: AspNetUserClaims PK_AspNetUserClaims; Type: CONSTRAINT; Schema: public; Owner: ronny
--

ALTER TABLE ONLY public."AspNetUserClaims"
    ADD CONSTRAINT "PK_AspNetUserClaims" PRIMARY KEY ("Id");


--
-- TOC entry 2716 (class 2606 OID 16516)
-- Name: AspNetUserLogins PK_AspNetUserLogins; Type: CONSTRAINT; Schema: public; Owner: ronny
--

ALTER TABLE ONLY public."AspNetUserLogins"
    ADD CONSTRAINT "PK_AspNetUserLogins" PRIMARY KEY ("LoginProvider", "ProviderKey");


--
-- TOC entry 2719 (class 2606 OID 16529)
-- Name: AspNetUserRoles PK_AspNetUserRoles; Type: CONSTRAINT; Schema: public; Owner: ronny
--

ALTER TABLE ONLY public."AspNetUserRoles"
    ADD CONSTRAINT "PK_AspNetUserRoles" PRIMARY KEY ("UserId", "RoleId");


--
-- TOC entry 2721 (class 2606 OID 16547)
-- Name: AspNetUserTokens PK_AspNetUserTokens; Type: CONSTRAINT; Schema: public; Owner: ronny
--

ALTER TABLE ONLY public."AspNetUserTokens"
    ADD CONSTRAINT "PK_AspNetUserTokens" PRIMARY KEY ("UserId", "LoginProvider", "Name");


--
-- TOC entry 2724 (class 2606 OID 16577)
-- Name: AspNetUsers PK_AspNetUsers; Type: CONSTRAINT; Schema: public; Owner: ronny
--

ALTER TABLE ONLY public."AspNetUsers"
    ADD CONSTRAINT "PK_AspNetUsers" PRIMARY KEY ("Id");


--
-- TOC entry 2704 (class 2606 OID 16466)
-- Name: __EFMigrationsHistory PK___EFMigrationsHistory; Type: CONSTRAINT; Schema: public; Owner: ronny
--

ALTER TABLE ONLY public."__EFMigrationsHistory"
    ADD CONSTRAINT "PK___EFMigrationsHistory" PRIMARY KEY ("MigrationId");


--
-- TOC entry 2722 (class 1259 OID 16578)
-- Name: EmailIndex; Type: INDEX; Schema: public; Owner: ronny
--

CREATE INDEX "EmailIndex" ON public."AspNetUsers" USING btree ("NormalizedEmail");


--
-- TOC entry 2708 (class 1259 OID 16553)
-- Name: IX_AspNetRoleClaims_RoleId; Type: INDEX; Schema: public; Owner: ronny
--

CREATE INDEX "IX_AspNetRoleClaims_RoleId" ON public."AspNetRoleClaims" USING btree ("RoleId");


--
-- TOC entry 2711 (class 1259 OID 16555)
-- Name: IX_AspNetUserClaims_UserId; Type: INDEX; Schema: public; Owner: ronny
--

CREATE INDEX "IX_AspNetUserClaims_UserId" ON public."AspNetUserClaims" USING btree ("UserId");


--
-- TOC entry 2714 (class 1259 OID 16556)
-- Name: IX_AspNetUserLogins_UserId; Type: INDEX; Schema: public; Owner: ronny
--

CREATE INDEX "IX_AspNetUserLogins_UserId" ON public."AspNetUserLogins" USING btree ("UserId");


--
-- TOC entry 2717 (class 1259 OID 16557)
-- Name: IX_AspNetUserRoles_RoleId; Type: INDEX; Schema: public; Owner: ronny
--

CREATE INDEX "IX_AspNetUserRoles_RoleId" ON public."AspNetUserRoles" USING btree ("RoleId");


--
-- TOC entry 2707 (class 1259 OID 16554)
-- Name: RoleNameIndex; Type: INDEX; Schema: public; Owner: ronny
--

CREATE UNIQUE INDEX "RoleNameIndex" ON public."AspNetRoles" USING btree ("NormalizedName");


--
-- TOC entry 2725 (class 1259 OID 16579)
-- Name: UserNameIndex; Type: INDEX; Schema: public; Owner: ronny
--

CREATE UNIQUE INDEX "UserNameIndex" ON public."AspNetUsers" USING btree ("NormalizedUserName");


--
-- TOC entry 2726 (class 2606 OID 16491)
-- Name: AspNetRoleClaims FK_AspNetRoleClaims_AspNetRoles_RoleId; Type: FK CONSTRAINT; Schema: public; Owner: ronny
--

ALTER TABLE ONLY public."AspNetRoleClaims"
    ADD CONSTRAINT "FK_AspNetRoleClaims_AspNetRoles_RoleId" FOREIGN KEY ("RoleId") REFERENCES public."AspNetRoles"("Id") ON DELETE CASCADE;


--
-- TOC entry 2727 (class 2606 OID 16530)
-- Name: AspNetUserRoles FK_AspNetUserRoles_AspNetRoles_RoleId; Type: FK CONSTRAINT; Schema: public; Owner: ronny
--

ALTER TABLE ONLY public."AspNetUserRoles"
    ADD CONSTRAINT "FK_AspNetUserRoles_AspNetRoles_RoleId" FOREIGN KEY ("RoleId") REFERENCES public."AspNetRoles"("Id") ON DELETE CASCADE;


-- Completed on 2020-02-11 15:03:58

--
-- PostgreSQL database dump complete
--

