﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace sri.Controllers
{
    public class ErrorController : Controller
    {
        // GET: /<controller>/

        [Route("Error")]
        public IActionResult Error() 
        {

            var exceptionDetails = HttpContext.Features.Get<IExceptionHandlerPathFeature>();

            ViewBag.ExceptionPath = exceptionDetails.Path;
             
             
            ViewBag.ExceptionMessage = exceptionDetails.Error.InnerException.Message; 
            
            
            ViewBag.StackTrace = exceptionDetails.Error.StackTrace;
            ViewBag.Source = exceptionDetails.Error.Source;


            return View("Failed");

        }
    }
}
