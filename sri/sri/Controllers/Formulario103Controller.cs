﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using sri.Models;

//herramientas del usuario autenticado

using Microsoft.AspNetCore.Identity;
using sri.Data;
using Microsoft.Extensions.Configuration;


using Microsoft.AspNetCore.Authorization;

namespace sri.Controllers
{
    [Authorize]
    public class Formulario103Controller : Controller
    {
        private readonly declaracionesContext _context;

        //enlace con usuario
        private readonly UserManager<IdentityUser> _manager;

        public Formulario103Controller(UserManager<IdentityUser> manager,declaracionesContext context)
        {
            _context = context;

            //inicializa manager de usuario
            _manager = manager;
        }

        //obtener usuario actual
        private async Task<IdentityUser> GetCurrentUser()
        {
            return await _manager.GetUserAsync(HttpContext.User);
        }

        //probando

        // GET: Formulario103
        public async Task<IActionResult> Index()
        {

            //obtiene instancia de usuario actual
            IdentityUser user = await GetCurrentUser();
            //itera y referencia purchases existentes en base de datos
            var formularios = from f in _context.Formulario103
                            select f;
            //aplica clausula where para filtrar purchases por propietario
            formularios = formularios.Where(f => f.Userid == user.Id );

            //retorna lista filtrada para su renderizacion en "index.cshtml"
            return View(await formularios.AsNoTracking<Formulario103>().ToListAsync());

        }
        //get duplicado
        public async Task<IActionResult> Index2()
        {

            //obtiene instancia de usuario actual
            IdentityUser user = await GetCurrentUser();
            //itera y referencia purchases existentes en base de datos
            var formularios = from f in _context.Formulario103
                              select f;
            //aplica clausula where para filtrar purchases por propietario
            //formularios = formularios.Where(f => f.Userid == user.Id);

            //retorna lista filtrada para su renderizacion en "index.cshtml"
            return View(await formularios.AsNoTracking<Formulario103>().ToListAsync());

        }

        // GET: Formulario103/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var formulario103 = await _context.Formulario103
                .FirstOrDefaultAsync(m => m.Id == id);
            if (formulario103 == null)
            {
                return NotFound();
            }

            return View(formulario103);
        }

        // GET: Formulario103/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Formulario103/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Userid,C101,C102,C104,C201,C202,C302,C352,C303,C353,C304,C354,C307,C357,C308,C358,C309,C359,C310,C360,C311,C361,C312,C362,C314,C364,C319,C369,C320,C370,C322,C372,C323,C373,C324,C374,C325,C375,C326,C376,C327,C377,C328,C378,C329,C379,C330,C380,C331,C332,C333,C383,C334,C384,C335,C385,C336,C386,C510,C520,C530,C540,C550,C337,C387,C338,C388,C339,C389,C340,C390,C341,C391,C342,C392,C343,C393,C344,C394,C345,C395,C346,C396,C349,C399,C402,C452,C403,C453,C404,C454,C405,C406,C456,C407,C457,C408,C458,C409,C459,C410,C460,C411,C461,C412,C413,C463,C414,C464,C415,C465,C416,C417,C467,C418,C468,C419,C469,C420,C470,C421,C471,C422,C472,C423,C424,C474,C425,C475,C426,C476,C427,C477,C428,C478,C429,C479,C430,C480,C431,C481,C432,C482,C433,C497,C498,C499,C890,C897,C898,C899,C880,C902,C903,C904,C999,C905,C907,C908,C909,C910,C911,C812,C813,C815,C198,C199")] Formulario103 formulario103)
        {
            if (ModelState.IsValid)
            {
                IdentityUser user = await GetCurrentUser();

                formulario103.Userid = user.Id;

                _context.Add(formulario103);

                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(formulario103);
        }

        // GET: Formulario103/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var formulario103 = await _context.Formulario103.FindAsync(id);
            if (formulario103 == null)
            {
                return NotFound();
            }
            return View(formulario103);
        }

        // POST: Formulario103/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Userid,C101,C102,C104,C201,C202,C302,C352,C303,C353,C304,C354,C307,C357,C308,C358,C309,C359,C310,C360,C311,C361,C312,C362,C314,C364,C319,C369,C320,C370,C322,C372,C323,C373,C324,C374,C325,C375,C326,C376,C327,C377,C328,C378,C329,C379,C330,C380,C331,C332,C333,C383,C334,C384,C335,C385,C336,C386,C510,C520,C530,C540,C550,C337,C387,C338,C388,C339,C389,C340,C390,C341,C391,C342,C392,C343,C393,C344,C394,C345,C395,C346,C396,C349,C399,C402,C452,C403,C453,C404,C454,C405,C406,C456,C407,C457,C408,C458,C409,C459,C410,C460,C411,C461,C412,C413,C463,C414,C464,C415,C465,C416,C417,C467,C418,C468,C419,C469,C420,C470,C421,C471,C422,C472,C423,C424,C474,C425,C475,C426,C476,C427,C477,C428,C478,C429,C479,C430,C480,C431,C481,C432,C482,C433,C497,C498,C499,C890,C897,C898,C899,C880,C902,C903,C904,C999,C905,C907,C908,C909,C910,C911,C812,C813,C815,C198,C199")] Formulario103 formulario103)
        {
            if (id != formulario103.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {

                    IdentityUser user = await GetCurrentUser();

                    formulario103.Userid = user.Id;

                    _context.Update(formulario103);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!Formulario103Exists(formulario103.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(formulario103);
        }

        // GET: Formulario103/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var formulario103 = await _context.Formulario103
                .FirstOrDefaultAsync(m => m.Id == id);
            if (formulario103 == null)
            {
                return NotFound();
            }

            return View(formulario103);
        }

        // POST: Formulario103/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var formulario103 = await _context.Formulario103.FindAsync(id);
            _context.Formulario103.Remove(formulario103);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool Formulario103Exists(int id)
        {
            return _context.Formulario103.Any(e => e.Id == id);
        }
    }
}
