﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using sri.Models;


//herramientas del usuario autenticado

using Microsoft.AspNetCore.Identity;
using sri.Data;
using Microsoft.Extensions.Configuration;


using Microsoft.AspNetCore.Authorization;


namespace sri.Controllers
{
    [Authorize]
    public class Formulario102aController : Controller
    {
        private readonly declaracionesContext _context;


        private readonly UserManager<IdentityUser> _manager;

        public Formulario102aController(UserManager<IdentityUser> manager,declaracionesContext context)
        {

            //inicializa manager de usuario
            _manager = manager;
            _context = context;
        }


        //obtener usuario actual
        private async Task<IdentityUser> GetCurrentUser()
        {
            return await _manager.GetUserAsync(HttpContext.User);
        }


        // GET: Formulario102a
        public async Task<IActionResult> Index()
        {

            //obtiene instancia de usuario actual
            IdentityUser user = await GetCurrentUser();
            //itera y referencia purchases existentes en base de datos
            var formularios = from f in _context.Formulario102a
                              select f;
            //aplica clausula where para filtrar purchases por propietario
            formularios = formularios.Where(f => f.Userid == user.Id);

            //envia lista filtrada para su renderizacion en "index.cshtml"
            return View(await formularios.AsNoTracking<Formulario102a>().ToListAsync());
        }

        public async Task<IActionResult> Index2()
        {

            //obtiene instancia de usuario actual
            IdentityUser user = await GetCurrentUser();
            //itera y referencia purchases existentes en base de datos
            var formularios = from f in _context.Formulario102a
                              select f;
            //aplica clausula where para filtrar purchases por propietario
            //formularios = formularios.Where(f => f.Userid == user.Id);

            //retorna lista filtrada para su renderizacion en "index.cshtml"
            return View(await formularios.AsNoTracking<Formulario102a>().ToListAsync());

        }

        // GET: Formulario102a/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var formulario102a = await _context.Formulario102a
                .FirstOrDefaultAsync(m => m.Id == id);
            if (formulario102a == null)
            {
                return NotFound();
            }

            return View(formulario102a);
        }

        // GET: Formulario102a/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Formulario102a/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Userid,C102,C104,C105,C201,C202,C703,C704,C705,C481,C710,C711,C712,C713,C714,C715,C716,C717,C718,C719,C720,C730,C729,C741,C491,C721,C722,C723,C724,C725,C731,C739,C751,C749,C759,C769,C740,C750,C760,C770,C768,C767,C771,C772,C773,C774,C775,C776,C777,C778,C779,C780,C781,C782,C783,C784,C786,C787,C789,C7004,C7006,C7005,C7007,C832,C839,C840,C842,C843,C845,C846,C847,C848,C849,C850,C851,C852,C855,C856,C857,C858,C859,C869,C880,C881,C882,C879,C871,C872,C890,C897,C898,C899,C902,C903,C904,C999,C905,C906,C907,C908,C909,C910,C911,C912,C913,C915,C916,C917,C918,C919,C920,Nombre,C198")] Formulario102a formulario102a)
        {
            if (ModelState.IsValid)
            {
                IdentityUser user = await GetCurrentUser();

                formulario102a.Userid = user.Id ;

                _context.Add(formulario102a);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(formulario102a);
        }

        // GET: Formulario102a/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var formulario102a = await _context.Formulario102a.FindAsync(id);
            if (formulario102a == null)
            {
                return NotFound();
            }
            return View(formulario102a);
        }

        // POST: Formulario102a/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Userid,C102,C104,C105,C201,C202,C703,C704,C705,C481,C710,C711,C712,C713,C714,C715,C716,C717,C718,C719,C720,C730,C729,C741,C491,C721,C722,C723,C724,C725,C731,C739,C751,C749,C759,C769,C740,C750,C760,C770,C768,C767,C771,C772,C773,C774,C775,C776,C777,C778,C779,C780,C781,C782,C783,C784,C786,C787,C789,C7004,C7006,C7005,C7007,C832,C839,C840,C842,C843,C845,C846,C847,C848,C849,C850,C851,C852,C855,C856,C857,C858,C859,C869,C880,C881,C882,C879,C871,C872,C890,C897,C898,C899,C902,C903,C904,C999,C905,C906,C907,C908,C909,C910,C911,C912,C913,C915,C916,C917,C918,C919,C920,Nombre,C198")] Formulario102a formulario102a)
        {
            if (id != formulario102a.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    IdentityUser user = await GetCurrentUser();

                    formulario102a.Userid = user.Id ;

                    _context.Update(formulario102a);

                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!Formulario102aExists(formulario102a.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(formulario102a);
        }

        // GET: Formulario102a/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var formulario102a = await _context.Formulario102a
                .FirstOrDefaultAsync(m => m.Id == id);
            if (formulario102a == null)
            {
                return NotFound();
            }

            return View(formulario102a);
        }

        // POST: Formulario102a/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var formulario102a = await _context.Formulario102a.FindAsync(id);
            _context.Formulario102a.Remove(formulario102a);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool Formulario102aExists(int id)
        {
            return _context.Formulario102a.Any(e => e.Id == id);
        }
    }
}
