﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using sri.Models;


//herramientas del usuario autenticado

using Microsoft.AspNetCore.Identity;
using sri.Data;
using Microsoft.Extensions.Configuration;


using Microsoft.AspNetCore.Authorization;

namespace sri.Controllers
{
    [Authorize]
    public class Formulario106Controller : Controller
    {
        //enlace con usuario
        private readonly UserManager<IdentityUser> _manager;

        private readonly declaracionesContext _context;

        public Formulario106Controller(UserManager<IdentityUser> manager, declaracionesContext context)
        {
            _context = context;

            //inicializa manager de usuario
            _manager = manager;
        }



        //obtener usuario actual
        private async Task<IdentityUser> GetCurrentUser()
        {
            return await _manager.GetUserAsync(HttpContext.User);
        }

        // GET: Formulario106
        public async Task<IActionResult> Index()
        {
            //obtiene instancia de usuario actual
            IdentityUser user = await GetCurrentUser();
            //itera y referencia purchases existentes en base de datos
            var formularios = from f in _context.Formulario106
                              select f;
            //aplica clausula where para filtrar purchases por propietario
            formularios = formularios.Where(p => p.Userid == user.Id);

            //envia lista filtrada para su renderizacion en "index.cshtml"
            return View(await formularios.AsNoTracking<Formulario106>().ToListAsync());
        }

        //get duplicado
        public async Task<IActionResult> Index2()
        {

            //obtiene instancia de usuario actual
            IdentityUser user = await GetCurrentUser();
            //itera y referencia purchases existentes en base de datos
            var formularios = from f in _context.Formulario106
                              select f;
            //aplica clausula where para filtrar purchases por propietario
            //formularios = formularios.Where(f => f.Userid == user.Id);

            //retorna lista filtrada para su renderizacion en "index.cshtml"
            return View(await formularios.AsNoTracking<Formulario106>().ToListAsync());

        }


        // GET: Formulario106/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var formulario106 = await _context.Formulario106
                .FirstOrDefaultAsync(m => m.Id == id);
            if (formulario106 == null)
            {
                return NotFound();
            }

            return View(formulario106);
        }

        // GET: Formulario106/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Formulario106/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Userid,C101,C102,C201,C202,C203,C204,C205,C301,C302,C303,C304,C305,C306,C198,C199,C902,C903,C904,C999,C905,C906,C907,C925,C908,C909,C910,C911,C912,C913,C914,C915,C916,C917,C918,C919,C920")] Formulario106 formulario106)
        {
            if (ModelState.IsValid)
            {
                IdentityUser user = await GetCurrentUser();

                formulario106.Userid = user.Id ;

                _context.Add(formulario106);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(formulario106);
        }

        // GET: Formulario106/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var formulario106 = await _context.Formulario106.FindAsync(id);
            if (formulario106 == null)
            {
                return NotFound();
            }
            return View(formulario106);
        }

        // POST: Formulario106/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Userid,C101,C102,C201,C202,C203,C204,C205,C301,C302,C303,C304,C305,C306,C198,C199,C902,C903,C904,C999,C905,C906,C907,C925,C908,C909,C910,C911,C912,C913,C914,C915,C916,C917,C918,C919,C920")] Formulario106 formulario106)
        {
            if (id != formulario106.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    IdentityUser user = await GetCurrentUser();

                    formulario106.Userid = user.Id ;

                    _context.Update(formulario106);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!Formulario106Exists(formulario106.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(formulario106);
        }

        // GET: Formulario106/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var formulario106 = await _context.Formulario106
                .FirstOrDefaultAsync(m => m.Id == id);
            if (formulario106 == null)
            {
                return NotFound();
            }

            return View(formulario106);
        }

        // POST: Formulario106/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var formulario106 = await _context.Formulario106.FindAsync(id);
            _context.Formulario106.Remove(formulario106);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool Formulario106Exists(int id)
        {
            return _context.Formulario106.Any(e => e.Id == id);
        }
    }
}
