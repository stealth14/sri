﻿/* 
 inputs de forma 103
*/

let C303 = document.getElementById("C303");
let C353 = document.getElementById("C353");

/* porcentajes de retencion a aplicar segun anexo del reglamento*/
let anexo303 = 0.1;

let anexo304ABCDE = 0.8; 
let anexo307 = 0.2;
let anexo308 = 0.1;
let anexo309 = 0.01;
let anexo310 = 0.01;
let anexo311 = 0.02;

/**
 * 
 * @param {any} porcentaje
 * @param {any} base
 */
function calcular_retencion(porcentaje,base)
{
    valorRetenido = porcentaje * base;
    //no remover parseInt hasta corregir problema con decimales de vistas asp
    return  parseInt(valorRetenido);
}


/* operaciones sobre nodos del dom*/
 
/**
 * 
 * @param {any} input
 * @param {any} value
 */

C303.onfocusout = function ()
{
    /*no olvidar castear todo a entero*/
    C353.value = calcular_retencion(anexo303,C303.value);
};

C307.onfocusout = function () {
    /*no olvidar castear todo a entero*/
    C357.value = calcular_retencion(anexo307, C307.value);
};

C308.onfocusout = function () {
    /*no olvidar castear todo a entero*/
    C358.value = calcular_retencion(anexo308, C308.value);
};

C309.onfocusout = function () {
    /*no olvidar castear todo a entero*/
    C359.value = calcular_retencion(anexo309, C309.value);
};

C310.onfocusout = function () {
    /*no olvidar castear todo a entero*/
    C360.value = calcular_retencion(anexo310, C310.value);
};

C311.onfocusout = function () {
    /*no olvidar castear todo a entero*/
    C361.value = calcular_retencion(anexo311, C311.value);
};

console.log("autollenado funcionando!!!")


