﻿using System.ComponentModel.DataAnnotations;

using System;
using System.Collections.Generic;

namespace sri.Models
{
    public partial class Formulario103
    {

        public int Id { get; set; }
        public string Userid { get; set; }

        //encabezado
        [Display(Name = "No. Mes")]
        public int? C101 { get; set; }

        [Display(Name = "Año")]
        public int? C102 { get; set; }

        [Display(Name = "No. Formulario que sustituye")]
        public int? C104 { get; set; }

        [Display(Name = "RUC")]
        public int? C201 { get; set; }

        [Display(Name = "Razon Social")]
        public int? C202 { get; set; }

        //por pagos efectuados a residentes y establecimientos permanentes


        [Display(Name = "Relac Dependencia")]
        public int? C302 { get; set; }
        [Display(Name = "Valor retenido")]
        //valor retenido
        public int? C352 { get; set; }

        [Display(Name = "Honorarios Profesionales")]
        public int? C303 { get; set; }
        [Display(Name = "Valor retenido")]
        //valor retenido
        public int? C353 { get; set; }

        [Display(Name = "Predomina Intelecto")]
        public int? C304 { get; set; }
        [Display(Name = "Valor retenido")]
        //valor retenido
        public int? C354 { get; set; }
        [Display(Name = "Predomina mano de obra")]
        public int? C307 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C357 { get; set; }
        [Display(Name = "Utilización o aprovechamiento de la imagen o renombre")]
        public int? C308 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C358 { get; set; }
        [Display(Name = "Publicidad y comunicación")]
        public int? C309 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C359 { get; set; }
        [Display(Name = "Transporte Privado de pasajeros o servicio publico o privado de carga")]
        public int? C310 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C360 { get; set; }
        [Display(Name = "A través de liquidaciones de compra")]
        public int? C311 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C361 { get; set; }
        [Display(Name = "Transferencia de bienes muebles de naturaleza corporal")]
        public int? C312 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C362 { get; set; }
        [Display(Name = "Por regalías, derechos de autor, marcas y patentes")]
        public int? C314 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C364 { get; set; }
        [Display(Name = "Mercantil")]
        public int? C319 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C369 { get; set; }
        [Display(Name = "Bienes inmuebles")]
        public int? C320 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C370 { get; set; }
        [Display(Name = "Seguros y reaseguros")]
        public int? C322 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C372 { get; set; }
        [Display(Name = "Rendimientos financieros")]
        public int? C323 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C373 { get; set; }
        [Display(Name = "Rendimientos entre instituciones")]
        public int? C324 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C374 { get; set; }
        [Display(Name = "Anticipo dividendos")]
        public int? C325 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C375 { get; set; }
        [Display(Name = "Dividendos Imp. renta")]
        public int? C326 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C376 { get; set; }
        [Display(Name = "Dividendos Personas naturales")]
        public int? C327 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C377 { get; set; }
        [Display(Name = "Dividendos sociedades residentes")]
        public int? C328 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C378 { get; set; }
        [Display(Name = "Dividendos fideicomisos")]
        public int? C329 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C379 { get; set; }
        [Display(Name = "Dividendos gravados")]
        public int? C330 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C380 { get; set; }
        [Display(Name = "Dividendos exentos")]
        public int? C331 { get; set; }
        [Display(Name = "Pagos de bienes o servicios")]
        public int? C332 { get; set; }
        [Display(Name = "Enajenacion cotizada")]
        public int? C333 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C383 { get; set; }
        [Display(Name = "Enajenacion no cotizada")]
        public int? C334 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C384 { get; set; }
        [Display(Name = "Loterias, rifas y apuestas")]
        public int? C335 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C385 { get; set; }
        [Display(Name = "A comercializadoras")]
        public int? C336 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C386 { get; set; }
        [Display(Name = "A distribuidores")]
        public int? C337 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C387 { get; set; }
        [Display(Name = "Compra local de banano")]
        public int? C510 { get; set; }
        public int? C338 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C388 { get; set; }
        [Display(Name = "Liquidacion banano")]
        public int? C520 { get; set; }
        public int? C339 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C389 { get; set; }
        [Display(Name = "Impuesto unico de banano 1")]
        public int? C530 { get; set; }
        [Display(Name = "Base imponible")]
        public int? C340 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C390 { get; set; }
        [Display(Name = "Impuesto unico de banano 2")]
        public int? C540 { get; set; }
        [Display(Name = "Base imponible")]
        public int? C341 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C391 { get; set; }
        [Display(Name = "Impuesto unico de banano terceros")]
        public int? C550 { get; set; }
        [Display(Name = "Base imponible")]
        public int? C342 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C392 { get; set; }
        [Display(Name = "Aplicables el 1%")]
        public int? C343 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C393 { get; set; }
        [Display(Name = "Aplicables el 2%")]
        public int? C344 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C394 { get; set; }
        [Display(Name = "Aplicables el 8%")]
        public int? C345 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C395 { get; set; }
        [Display(Name = "Aplicables a otros porcentajes")]
        public int? C346 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C396 { get; set; }
        [Display(Name = "Subtotal operaciones efectuadas")]
        public int? C349 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C399 { get; set; }

        //por pagos efectuados a residentes y establecimientos permanentes

        [Display(Name = "Int. Financiamiento")]
        public int? C402 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C452 { get; set; }
        [Display(Name = "Int. creditos")]
        public int? C403 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C453 { get; set; }
        [Display(Name = "Anticipo de dividendos")]
        public int? C404 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C454 { get; set; }
        [Display(Name = "Dividendos personas naturales")]
        public int? C405 { get; set; }
        [Display(Name = "Dividendos sociedades")]
        public int? C406 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C456 { get; set; }
        [Display(Name = "Dividendos fideicomisos")]
        public int? C407 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C457 { get; set; }
        [Display(Name = "Enajenacion de derechos")]
        public int? C408 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C458 { get; set; }
        [Display(Name = "Seguros y reaseguros")]
        public int? C409 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C459 { get; set; }
        [Display(Name = "Servicios tecnicos")]
        public int? C410 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C460 { get; set; }
        [Display(Name = "Ingresos gravados")]
        public int? C411 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C461 { get; set; }
        [Display(Name = "Pagos sujetos a retencion")]
        public int? C412 { get; set; }
        [Display(Name = "Interes por proveedores")]
        public int? C413 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C463 { get; set; }
        [Display(Name = "Intereses de creditos")]
        public int? C414 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C464 { get; set; }
        [Display(Name = "Anticipo de dividendos")]
        public int? C415 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C465 { get; set; }
        [Display(Name = "Dividendos personas naturales")]
        public int? C416 { get; set; }
        [Display(Name = "Dividendos sociedades")]
        public int? C417 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C467 { get; set; }
        [Display(Name = "Dividendos fideicomisos")]
        public int? C418 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C468 { get; set; }
        [Display(Name = "Enajenacion de derechos")]
        public int? C419 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C469 { get; set; }
        [Display(Name = "Seguros y reaseguros")]
        public int? C420 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C470 { get; set; }
        [Display(Name = "Servicios tecnicos")]
        public int? C421 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C471 { get; set; }
        [Display(Name = "Ingresos gravados")]
        public int? C422 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C472 { get; set; }
        [Display(Name = "Pagos sujetos a retencion")]
        public int? C423 { get; set; }
        [Display(Name = "Intereses")]
        public int? C424 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C474 { get; set; }
        [Display(Name = "Anticipo de dividendos")]
        public int? C425 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C475 { get; set; }
        [Display(Name = "Dividendos personas naturales")]
        public int? C426 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C476 { get; set; }
        [Display(Name = "Dividendos sociedades")]
        public int? C427 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C477 { get; set; }
        [Display(Name = "Dividendos fideicomisos")]
        public int? C428 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C478 { get; set; }
        [Display(Name = "Enajenacion de derechos")]
        public int? C429 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C479 { get; set; }
        [Display(Name = "Seguros y reaseguros")]
        public int? C430 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C480 { get; set; }
        [Display(Name = "Servicios tecnicos")]
        public int? C431 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C481 { get; set; }
        [Display(Name = "Ingresos gravados")]
        public int? C432 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C482 { get; set; }
        [Display(Name = "Pagos no sujetos a retencion")]
        public int? C433 { get; set; }
        [Display(Name = "Subtotal de operaciones efectuadas con el exterior")]
        public int? C497 { get; set; }
        [Display(Name = "Valor retenido")]
        public int? C498 { get; set; }
        [Display(Name = "Total de retencion de impuesto a la renta")]
        public int? C499 { get; set; }
        [Display(Name = "Pago previo")]
        public int? C890 { get; set; }
        [Display(Name = "Interes")]
        public int? C897 { get; set; }
        [Display(Name = "Impuesto")]
        public int? C898 { get; set; }
        [Display(Name = "Multa")]
        public int? C899 { get; set; }
        [Display(Name = "Pago directo")]
        public int? C880 { get; set; }
        [Display(Name = "Total impuesto a pagar")]
        public int? C902 { get; set; }
        [Display(Name = "Interes por mora")]
        public int? C903 { get; set; }
        [Display(Name = "Multa")]
        public int? C904 { get; set; }
        [Display(Name = "Total pagado")]
        public int? C999 { get; set; }
        [Display(Name = "Mediante cheque")]
        public int? C905 { get; set; }
        [Display(Name = "Mediante notas de credito")]
        public int? C907 { get; set; }
        [Display(Name = "Detalle de notas de credito cartulares")]
        public int? C908 { get; set; }
        public int? C909 { get; set; }
        public int? C910 { get; set; }
        public int? C911 { get; set; }
        public int? C812 { get; set; }
        public int? C813 { get; set; }
        [Display(Name = "Detalle de notas desmaterializadas")]
        public int? C815 { get; set; }
        [Display(Name = "Cedula de Identidad")]
        public int? C198 { get; set; }
        [Display(Name = "Ruc")]
        public int? C199 { get; set; }
    }
}
