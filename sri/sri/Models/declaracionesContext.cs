﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using sri.Models;

namespace sri.Models
{
    public partial class declaracionesContext : DbContext
    {
        public declaracionesContext()
        {
        }

        public declaracionesContext(DbContextOptions<declaracionesContext> options)
            : base(options)
        {
        }
        public virtual DbSet<Auditoria> Auditoria { get; set; }
        public virtual DbSet<Formulario103> Formulario103 { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Data Source=DESKTOP-8N9QUA0; Initial Catalog=declaraciones; User Id=ronny; Password=naty");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Formulario103>(entity =>
            {

                entity.ToTable("formulario103");

                entity.Property(e => e.C101).HasColumnName("c101");

                entity.Property(e => e.C102).HasColumnName("c102");

                entity.Property(e => e.C104).HasColumnName("c104");

                entity.Property(e => e.C198).HasColumnName("c198");

                entity.Property(e => e.C199).HasColumnName("c199");

                entity.Property(e => e.C201).HasColumnName("c201");

                entity.Property(e => e.C202).HasColumnName("c202");

                entity.Property(e => e.C302).HasColumnName("c302");

                entity.Property(e => e.C303).HasColumnName("c303");

                entity.Property(e => e.C304).HasColumnName("c304");

                entity.Property(e => e.C307).HasColumnName("c307");

                entity.Property(e => e.C308).HasColumnName("c308");

                entity.Property(e => e.C309).HasColumnName("c309");

                entity.Property(e => e.C310).HasColumnName("c310");

                entity.Property(e => e.C311).HasColumnName("c311");

                entity.Property(e => e.C312).HasColumnName("c312");

                entity.Property(e => e.C314).HasColumnName("c314");

                entity.Property(e => e.C319).HasColumnName("c319");

                entity.Property(e => e.C320).HasColumnName("c320");

                entity.Property(e => e.C322).HasColumnName("c322");

                entity.Property(e => e.C323).HasColumnName("c323");

                entity.Property(e => e.C324).HasColumnName("c324");

                entity.Property(e => e.C325).HasColumnName("c325");

                entity.Property(e => e.C326).HasColumnName("c326");

                entity.Property(e => e.C327).HasColumnName("c327");

                entity.Property(e => e.C328).HasColumnName("c328");

                entity.Property(e => e.C329).HasColumnName("c329");

                entity.Property(e => e.C330).HasColumnName("c330");

                entity.Property(e => e.C331).HasColumnName("c331");

                entity.Property(e => e.C332).HasColumnName("c332");

                entity.Property(e => e.C333).HasColumnName("c333");

                entity.Property(e => e.C334).HasColumnName("c334");

                entity.Property(e => e.C335).HasColumnName("c335");

                entity.Property(e => e.C336).HasColumnName("c336");

                entity.Property(e => e.C337).HasColumnName("c337");

                entity.Property(e => e.C338).HasColumnName("c338");

                entity.Property(e => e.C339).HasColumnName("c339");

                entity.Property(e => e.C340).HasColumnName("c340");

                entity.Property(e => e.C341).HasColumnName("c341");

                entity.Property(e => e.C342).HasColumnName("c342");

                entity.Property(e => e.C343).HasColumnName("c343");

                entity.Property(e => e.C344).HasColumnName("c344");

                entity.Property(e => e.C345).HasColumnName("c345");

                entity.Property(e => e.C346).HasColumnName("c346");

                entity.Property(e => e.C349).HasColumnName("c349");

                entity.Property(e => e.C352).HasColumnName("c352");

                entity.Property(e => e.C353).HasColumnName("c353");

                entity.Property(e => e.C354).HasColumnName("c354");

                entity.Property(e => e.C357).HasColumnName("c357");

                entity.Property(e => e.C358).HasColumnName("c358");

                entity.Property(e => e.C359).HasColumnName("c359");

                entity.Property(e => e.C360).HasColumnName("c360");

                entity.Property(e => e.C361).HasColumnName("c361");

                entity.Property(e => e.C362).HasColumnName("c362");

                entity.Property(e => e.C364).HasColumnName("c364");

                entity.Property(e => e.C369).HasColumnName("c369");

                entity.Property(e => e.C370).HasColumnName("c370");

                entity.Property(e => e.C372).HasColumnName("c372");

                entity.Property(e => e.C373).HasColumnName("c373");

                entity.Property(e => e.C374).HasColumnName("c374");

                entity.Property(e => e.C375).HasColumnName("c375");

                entity.Property(e => e.C376).HasColumnName("c376");

                entity.Property(e => e.C377).HasColumnName("c377");

                entity.Property(e => e.C378).HasColumnName("c378");

                entity.Property(e => e.C379).HasColumnName("c379");

                entity.Property(e => e.C380).HasColumnName("c380");

                entity.Property(e => e.C383).HasColumnName("c383");

                entity.Property(e => e.C384).HasColumnName("c384");

                entity.Property(e => e.C385).HasColumnName("c385");

                entity.Property(e => e.C386).HasColumnName("c386");

                entity.Property(e => e.C387).HasColumnName("c387");

                entity.Property(e => e.C388).HasColumnName("c388");

                entity.Property(e => e.C389).HasColumnName("c389");

                entity.Property(e => e.C390).HasColumnName("c390");

                entity.Property(e => e.C391).HasColumnName("c391");

                entity.Property(e => e.C392).HasColumnName("c392");

                entity.Property(e => e.C393).HasColumnName("c393");

                entity.Property(e => e.C394).HasColumnName("c394");

                entity.Property(e => e.C395).HasColumnName("c395");

                entity.Property(e => e.C396).HasColumnName("c396");

                entity.Property(e => e.C399).HasColumnName("c399");

                entity.Property(e => e.C402).HasColumnName("c402");

                entity.Property(e => e.C403).HasColumnName("c403");

                entity.Property(e => e.C404).HasColumnName("c404");

                entity.Property(e => e.C405).HasColumnName("c405");

                entity.Property(e => e.C406).HasColumnName("c406");

                entity.Property(e => e.C407).HasColumnName("c407");

                entity.Property(e => e.C408).HasColumnName("c408");

                entity.Property(e => e.C409).HasColumnName("c409");

                entity.Property(e => e.C410).HasColumnName("c410");

                entity.Property(e => e.C411).HasColumnName("c411");

                entity.Property(e => e.C412).HasColumnName("c412");

                entity.Property(e => e.C413).HasColumnName("c413");

                entity.Property(e => e.C414).HasColumnName("c414");

                entity.Property(e => e.C415).HasColumnName("c415");

                entity.Property(e => e.C416).HasColumnName("c416");

                entity.Property(e => e.C417).HasColumnName("c417");

                entity.Property(e => e.C418).HasColumnName("c418");

                entity.Property(e => e.C419).HasColumnName("c419");

                entity.Property(e => e.C420).HasColumnName("c420");

                entity.Property(e => e.C421).HasColumnName("c421");

                entity.Property(e => e.C422).HasColumnName("c422");

                entity.Property(e => e.C423).HasColumnName("c423");

                entity.Property(e => e.C424).HasColumnName("c424");

                entity.Property(e => e.C425).HasColumnName("c425");

                entity.Property(e => e.C426).HasColumnName("c426");

                entity.Property(e => e.C427).HasColumnName("c427");

                entity.Property(e => e.C428).HasColumnName("c428");

                entity.Property(e => e.C429).HasColumnName("c429");

                entity.Property(e => e.C430).HasColumnName("c430");

                entity.Property(e => e.C431).HasColumnName("c431");

                entity.Property(e => e.C432).HasColumnName("c432");

                entity.Property(e => e.C433).HasColumnName("c433");

                entity.Property(e => e.C452).HasColumnName("c452");

                entity.Property(e => e.C453).HasColumnName("c453");

                entity.Property(e => e.C454).HasColumnName("c454");

                entity.Property(e => e.C456).HasColumnName("c456");

                entity.Property(e => e.C457).HasColumnName("c457");

                entity.Property(e => e.C458).HasColumnName("c458");

                entity.Property(e => e.C459).HasColumnName("c459");

                entity.Property(e => e.C460).HasColumnName("c460");

                entity.Property(e => e.C461).HasColumnName("c461");

                entity.Property(e => e.C463).HasColumnName("c463");

                entity.Property(e => e.C464).HasColumnName("c464");

                entity.Property(e => e.C465).HasColumnName("c465");

                entity.Property(e => e.C467).HasColumnName("c467");

                entity.Property(e => e.C468).HasColumnName("c468");

                entity.Property(e => e.C469).HasColumnName("c469");

                entity.Property(e => e.C470).HasColumnName("c470");

                entity.Property(e => e.C471).HasColumnName("c471");

                entity.Property(e => e.C472).HasColumnName("c472");

                entity.Property(e => e.C474).HasColumnName("c474");

                entity.Property(e => e.C475).HasColumnName("c475");

                entity.Property(e => e.C476).HasColumnName("c476");

                entity.Property(e => e.C477).HasColumnName("c477");

                entity.Property(e => e.C478).HasColumnName("c478");

                entity.Property(e => e.C479).HasColumnName("c479");

                entity.Property(e => e.C480).HasColumnName("c480");

                entity.Property(e => e.C481).HasColumnName("c481");

                entity.Property(e => e.C482).HasColumnName("c482");

                entity.Property(e => e.C497).HasColumnName("c497");

                entity.Property(e => e.C498).HasColumnName("c498");

                entity.Property(e => e.C499).HasColumnName("c499");

                entity.Property(e => e.C510).HasColumnName("c510");

                entity.Property(e => e.C520).HasColumnName("c520");

                entity.Property(e => e.C530).HasColumnName("c530");

                entity.Property(e => e.C540).HasColumnName("c540");

                entity.Property(e => e.C550).HasColumnName("c550");

                entity.Property(e => e.C812).HasColumnName("c812");

                entity.Property(e => e.C813).HasColumnName("c813");

                entity.Property(e => e.C815).HasColumnName("c815");

                entity.Property(e => e.C880).HasColumnName("c880");

                entity.Property(e => e.C890).HasColumnName("c890");

                entity.Property(e => e.C897).HasColumnName("c897");

                entity.Property(e => e.C898).HasColumnName("c898");

                entity.Property(e => e.C899).HasColumnName("c899");

                entity.Property(e => e.C902).HasColumnName("c902");

                entity.Property(e => e.C903).HasColumnName("c903");

                entity.Property(e => e.C904).HasColumnName("c904");

                entity.Property(e => e.C905).HasColumnName("c905");

                entity.Property(e => e.C907).HasColumnName("c907");

                entity.Property(e => e.C908).HasColumnName("c908");

                entity.Property(e => e.C909).HasColumnName("c909");

                entity.Property(e => e.C910).HasColumnName("c910");

                entity.Property(e => e.C911).HasColumnName("c911");

                entity.Property(e => e.C999).HasColumnName("c999");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Userid)
                    .HasColumnName("userid")
                    .HasMaxLength(450);
            });

            modelBuilder.Entity<Formulario102a>(entity =>
            {

                entity.ToTable("formulario102a");

                entity.Property(e => e.C102).HasColumnName("c102");

                entity.Property(e => e.C104).HasColumnName("c104");

                entity.Property(e => e.C105).HasColumnName("c105");

                entity.Property(e => e.C198).HasColumnName("c198");

                entity.Property(e => e.C201).HasColumnName("c201");

                entity.Property(e => e.C202)
                    .HasColumnName("c202")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.C481).HasColumnName("c481");

                entity.Property(e => e.C491).HasColumnName("c491");

                entity.Property(e => e.C7004)
                    .HasColumnName("c7004")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.C7005).HasColumnName("c7005");

                entity.Property(e => e.C7006)
                    .HasColumnName("c7006")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.C7007).HasColumnName("c7007");

                entity.Property(e => e.C703).HasColumnName("c703");

                entity.Property(e => e.C704).HasColumnName("c704");

                entity.Property(e => e.C705).HasColumnName("c705");

                entity.Property(e => e.C710).HasColumnName("c710");

                entity.Property(e => e.C711).HasColumnName("c711");

                entity.Property(e => e.C712).HasColumnName("c712");

                entity.Property(e => e.C713).HasColumnName("c713");

                entity.Property(e => e.C714).HasColumnName("c714");

                entity.Property(e => e.C715).HasColumnName("c715");

                entity.Property(e => e.C716).HasColumnName("c716");

                entity.Property(e => e.C717).HasColumnName("c717");

                entity.Property(e => e.C718).HasColumnName("c718");

                entity.Property(e => e.C719).HasColumnName("c719");

                entity.Property(e => e.C720).HasColumnName("c720");

                entity.Property(e => e.C721).HasColumnName("c721");

                entity.Property(e => e.C722).HasColumnName("c722");

                entity.Property(e => e.C723).HasColumnName("c723");

                entity.Property(e => e.C724).HasColumnName("c724");

                entity.Property(e => e.C725).HasColumnName("c725");

                entity.Property(e => e.C729).HasColumnName("c729");

                entity.Property(e => e.C730).HasColumnName("c730");

                entity.Property(e => e.C731).HasColumnName("c731");

                entity.Property(e => e.C739).HasColumnName("c739");

                entity.Property(e => e.C740)
                    .HasColumnName("c740")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.C741).HasColumnName("c741");

                entity.Property(e => e.C749).HasColumnName("c749");

                entity.Property(e => e.C750)
                    .HasColumnName("c750")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.C751).HasColumnName("c751");

                entity.Property(e => e.C759).HasColumnName("c759");

                entity.Property(e => e.C760)
                    .HasColumnName("c760")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.C767).HasColumnName("c767");

                entity.Property(e => e.C768).HasColumnName("c768");

                entity.Property(e => e.C769).HasColumnName("c769");

                entity.Property(e => e.C770)
                    .HasColumnName("c770")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.C771).HasColumnName("c771");

                entity.Property(e => e.C772).HasColumnName("c772");

                entity.Property(e => e.C773).HasColumnName("c773");

                entity.Property(e => e.C774).HasColumnName("c774");

                entity.Property(e => e.C775).HasColumnName("c775");

                entity.Property(e => e.C776).HasColumnName("c776");

                entity.Property(e => e.C777).HasColumnName("c777");

                entity.Property(e => e.C778).HasColumnName("c778");

                entity.Property(e => e.C779).HasColumnName("c779");

                entity.Property(e => e.C780).HasColumnName("c780");

                entity.Property(e => e.C781).HasColumnName("c781");

                entity.Property(e => e.C782).HasColumnName("c782");

                entity.Property(e => e.C783).HasColumnName("c783");

                entity.Property(e => e.C784).HasColumnName("c784");

                entity.Property(e => e.C786).HasColumnName("c786");

                entity.Property(e => e.C787).HasColumnName("c787");

                entity.Property(e => e.C789).HasColumnName("c789");

                entity.Property(e => e.C832).HasColumnName("c832");

                entity.Property(e => e.C839).HasColumnName("c839");

                entity.Property(e => e.C840).HasColumnName("c840");

                entity.Property(e => e.C842).HasColumnName("c842");

                entity.Property(e => e.C843).HasColumnName("c843");

                entity.Property(e => e.C845).HasColumnName("c845");

                entity.Property(e => e.C846).HasColumnName("c846");

                entity.Property(e => e.C847).HasColumnName("c847");

                entity.Property(e => e.C848).HasColumnName("c848");

                entity.Property(e => e.C849).HasColumnName("c849");

                entity.Property(e => e.C850).HasColumnName("c850");

                entity.Property(e => e.C851).HasColumnName("c851");

                entity.Property(e => e.C852).HasColumnName("c852");

                entity.Property(e => e.C855).HasColumnName("c855");

                entity.Property(e => e.C856).HasColumnName("c856");

                entity.Property(e => e.C857).HasColumnName("c857");

                entity.Property(e => e.C858).HasColumnName("c858");

                entity.Property(e => e.C859).HasColumnName("c859");

                entity.Property(e => e.C869).HasColumnName("c869");

                entity.Property(e => e.C871).HasColumnName("c871");

                entity.Property(e => e.C872).HasColumnName("c872");

                entity.Property(e => e.C879).HasColumnName("c879");

                entity.Property(e => e.C880).HasColumnName("c880");

                entity.Property(e => e.C881).HasColumnName("c881");

                entity.Property(e => e.C882).HasColumnName("c882");

                entity.Property(e => e.C890).HasColumnName("c890");

                entity.Property(e => e.C897).HasColumnName("c897");

                entity.Property(e => e.C898).HasColumnName("c898");

                entity.Property(e => e.C899).HasColumnName("c899");

                entity.Property(e => e.C902).HasColumnName("c902");

                entity.Property(e => e.C903).HasColumnName("c903");

                entity.Property(e => e.C904).HasColumnName("c904");

                entity.Property(e => e.C905).HasColumnName("c905");

                entity.Property(e => e.C906).HasColumnName("c906");

                entity.Property(e => e.C907).HasColumnName("c907");

                entity.Property(e => e.C908).HasColumnName("c908");

                entity.Property(e => e.C909).HasColumnName("c909");

                entity.Property(e => e.C910).HasColumnName("c910");

                entity.Property(e => e.C911).HasColumnName("c911");

                entity.Property(e => e.C912).HasColumnName("c912");

                entity.Property(e => e.C913).HasColumnName("c913");

                entity.Property(e => e.C915).HasColumnName("c915");

                entity.Property(e => e.C916).HasColumnName("c916");

                entity.Property(e => e.C917).HasColumnName("c917");

                entity.Property(e => e.C918).HasColumnName("c918");

                entity.Property(e => e.C919).HasColumnName("c919");

                entity.Property(e => e.C920).HasColumnName("c920");

                entity.Property(e => e.C999).HasColumnName("c999");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Nombre)
                    .HasColumnName("nombre")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Userid)
                    .HasColumnName("userid")
                    .HasMaxLength(450);
            });

            modelBuilder.Entity<Formulario106>(entity =>
            {

                entity.ToTable("formulario106");

                entity.Property(e => e.C101).HasColumnName("c101");

                entity.Property(e => e.C102).HasColumnName("c102");

                entity.Property(e => e.C198).HasColumnName("c198");

                entity.Property(e => e.C199).HasColumnName("c199");

                entity.Property(e => e.C201).HasColumnName("c201");

                entity.Property(e => e.C202)
                    .HasColumnName("c202")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.C203)
                    .HasColumnName("c203")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.C204)
                    .HasColumnName("c204")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.C205).HasColumnName("c205");

                entity.Property(e => e.C301).HasColumnName("c301");

                entity.Property(e => e.C302)
                    .HasColumnName("c302")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.C303).HasColumnName("c303");

                entity.Property(e => e.C304).HasColumnName("c304");

                entity.Property(e => e.C305).HasColumnName("c305");

                entity.Property(e => e.C306).HasColumnName("c306");

                entity.Property(e => e.C902).HasColumnName("c902");

                entity.Property(e => e.C903).HasColumnName("c903");

                entity.Property(e => e.C904).HasColumnName("c904");

                entity.Property(e => e.C905).HasColumnName("c905");

                entity.Property(e => e.C906).HasColumnName("c906");

                entity.Property(e => e.C907).HasColumnName("c907");

                entity.Property(e => e.C908).HasColumnName("c908");

                entity.Property(e => e.C909).HasColumnName("c909");

                entity.Property(e => e.C910).HasColumnName("c910");

                entity.Property(e => e.C911).HasColumnName("c911");

                entity.Property(e => e.C912).HasColumnName("c912");

                entity.Property(e => e.C913).HasColumnName("c913");

                entity.Property(e => e.C914).HasColumnName("c914");

                entity.Property(e => e.C915).HasColumnName("c915");

                entity.Property(e => e.C916).HasColumnName("c916");

                entity.Property(e => e.C917).HasColumnName("c917");

                entity.Property(e => e.C918).HasColumnName("c918");

                entity.Property(e => e.C919).HasColumnName("c919");

                entity.Property(e => e.C920).HasColumnName("c920");

                entity.Property(e => e.C925).HasColumnName("c925");

                entity.Property(e => e.C999).HasColumnName("c999");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Userid)
                    .HasColumnName("userid")
                    .HasMaxLength(450);
            });

            modelBuilder.Entity<Auditoria>(entity =>
            {

                entity.ToTable("auditoria");

                entity.Property(e => e.Fecha)
                    .HasColumnName("fecha")
                    .HasColumnType("date");

                entity.Property(e => e.Formulario)
                    .HasColumnName("formulario")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Operacion)
                    .HasColumnName("operacion")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Userid)
                    .HasColumnName("userid")
                    .HasMaxLength(450);

                entity.Property(e => e.Usermail)
                    .HasColumnName("usermail")
                    .HasMaxLength(100);
            });


            OnModelCreatingPartial(modelBuilder);


        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);

        public DbSet<sri.Models.Formulario106> Formulario106 { get; set; }

        public DbSet<sri.Models.Formulario102a> Formulario102a { get; set; }

        public DbSet<sri.Models.Auditoria> Auditoria_1 { get; set; }
    }
}
