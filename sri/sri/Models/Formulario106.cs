﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace sri.Models
{
    public partial class Formulario106
    {
        public int Id { get; set; }
        public string Userid { get; set; }

        [Display(Name = "Mes")]
        public int? C101 { get; set; }
        [Display(Name = "Año")]
        public int? C102 { get; set; }
        [Display(Name = "RUC")]
        public int? C201 { get; set; }
        [Display(Name = "Razon social")]
        public string C202 { get; set; }
        [Display(Name = "Ciudad")]
        public string C203 { get; set; }
        [Display(Name = "Calle principal")]
        public string C204 { get; set; }
        [Display(Name = "Numero")]
        public int? C205 { get; set; }
        [Display(Name = "Codigo impuesto")]
        public int? C301 { get; set; }
        [Display(Name = "Descripcion")]
        public string C302 { get; set; }
        [Display(Name = "Codigo documento")]
        public int? C303 { get; set; }
        [Display(Name = "N. Documento")]
        public int? C304 { get; set; }
        [Display(Name = "Cuota o número")]
        public int? C305 { get; set; }
        [Display(Name = "N. Formulario")]
        public int? C306 { get; set; }
        [Display(Name = "CI")]
        public int? C198 { get; set; }
        [Display(Name = "RUC")]
        public int? C199 { get; set; }
        [Display(Name = "Impuesto")]
        public int? C902 { get; set; }
        [Display(Name = "Intereses por mora")]
        public int? C903 { get; set; }
        [Display(Name = "Multas + recargos")]
        public int? C904 { get; set; }
        [Display(Name = "Total pagado")]
        public int? C999 { get; set; }
        [Display(Name = "Mediante cheque")]
        public int? C905 { get; set; }
        [Display(Name = "Mediante compensaciones")]
        public int? C906 { get; set; }
        [Display(Name = "Mediante notas de crédito")]
        public int? C907 { get; set; }
        [Display(Name = "Mediante títulos del Banco Central")]
        public int? C925 { get; set; }
        [Display(Name = "N/C No")]
        public int? C908 { get; set; }
        [Display(Name = "USD")]
        public int? C909 { get; set; }
        [Display(Name = "N/C No")]
        public int? C910 { get; set; }
        [Display(Name = "USD")]
        public int? C911 { get; set; }
        [Display(Name = "N/C No")]
        public int? C912 { get; set; }
        [Display(Name = "USD")]
        public int? C913 { get; set; }
        [Display(Name = "N/C No")]
        public int? C914 { get; set; }
        [Display(Name = "USD")]
        public int? C915 { get; set; }
        [Display(Name = "N/C No")]
        public int? C916 { get; set; }
        [Display(Name = "USD")]
        public int? C917 { get; set; }
        [Display(Name = "N/C No")]
        public int? C918 { get; set; }
        [Display(Name = "USD")]
        public int? C919 { get; set; }
        [Display(Name = "USD")]
        public int? C920 { get; set; }
    }
}
