﻿using System;
using System.Collections.Generic;

namespace sri.Models
{
    public partial class Auditoria
    {
        public string Operacion { get; set; }
        public string Formulario { get; set; }
        public DateTime? Fecha { get; set; }
        public string Userid { get; set; }
        public string Usermail { get; set; }
        public int Id { get; set; }
    }
}
