﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;



namespace sri.Models
{
    public partial class Formulario102a
    {
        public int Id { get; set; }
        public string Userid { get; set; }
        [Display(Name = "Año")]
        public int? C102 { get; set; }
        [Display(Name = "N. Formulario que sustituye")]
        public int? C104 { get; set; }
        [Display(Name = "N. Empleados en relacion de dependencia")]
        public int? C105 { get; set; }
        [Display(Name = "RUC")]
        public int? C201 { get; set; }
        [Display(Name = "Apellidos y nombres")]
        public string C202 { get; set; }
        [Display(Name = "Avalúo")]
        public int? C703 { get; set; }
        [Display(Name = "Avalúo")]
        public int? C704 { get; set; }
        [Display(Name = "Avalúo")]
        public int? C705 { get; set; }
        [Display(Name = "Ingresos")]
        public int? C481 { get; set; }
        [Display(Name = "Ingresos")]
        public int? C710 { get; set; }
        [Display(Name = "Ingresos")]
        public int? C711 { get; set; }
        [Display(Name = "Ingresos")]
        public int? C712 { get; set; }
        [Display(Name = "Ingresos")]
        public int? C713 { get; set; }
        [Display(Name = "Ingresos")]
        public int? C714 { get; set; }
        [Display(Name = "Ingresos")]
        public int? C715 { get; set; }
        [Display(Name = "Ingresos")]
        public int? C716 { get; set; }
        [Display(Name = "Ingresos")]
        public int? C717 { get; set; }
        [Display(Name = "Ingresos")]
        public int? C718 { get; set; }
        [Display(Name = "Ingresos")]
        public int? C719 { get; set; }
        [Display(Name = "Ingresos")]
        public int? C720 { get; set; }
        [Display(Name = "Ingresos")]
        public int? C730 { get; set; }
        [Display(Name = "Subtotal Ingresos")]
        public int? C729 { get; set; }
        [Display(Name = "Sueldos y salarios Ingresos")]
        public int? C741 { get; set; }
        [Display(Name = "Gastos deducibles")]
        public int? C491 { get; set; }
        [Display(Name = "Gastos deducibles")]
        public int? C721 { get; set; }
        [Display(Name = "Gastos deducibles")]
        public int? C722 { get; set; }
        [Display(Name = "Gastos deducibles")]
        public int? C723 { get; set; }
        [Display(Name = "Gastos deducibles")]
        public int? C724 { get; set; }
        [Display(Name = "Gastos deducibles")]
        public int? C725 { get; set; }
        [Display(Name = "Gastos deducibles")]
        public int? C731 { get; set; }
        [Display(Name = "Subtotal Gastos")]
        public int? C739 { get; set; }
        [Display(Name = "Sueldos y salarios Gastos")]
        public int? C751 { get; set; }
        [Display(Name = "Renta imponible")]
        public int? C749 { get; set; }
        [Display(Name = "Sueldos y salarios Renta")]
        public int? C759 { get; set; }
        [Display(Name = "Subtotal base gravada")]
        public int? C769 { get; set; }
        [Display(Name = "Tipo de beneficiario")]
        public string C740 { get; set; }
        [Display(Name = "Identificacion del discapacitado")]
        public string C750 { get; set; }
        [Display(Name = "Porcentaje de discapacidad")]
        public string C760 { get; set; }
        [Display(Name = "Identificación de cónyugue")]
        public string C770 { get; set; }
        [Display(Name = "Aplicable al periodo")]
        public int? C768 { get; set; }
        [Display(Name = "Aplicable al periodo")]
        public int? C767 { get; set; }
        [Display(Name = "Aplicable al periodo")]
        public int? C771 { get; set; }
        [Display(Name = "Aplicable al periodo")]
        public int? C772 { get; set; }
        [Display(Name = "Aplicable al periodo")]
        public int? C773 { get; set; }
        [Display(Name = "Aplicable al periodo")]
        public int? C774 { get; set; }
        [Display(Name = "Aplicable al periodo")]
        public int? C775 { get; set; }
        [Display(Name = "Aplicable al periodo")]
        public int? C776 { get; set; }
        [Display(Name = "Aplicable al periodo")]
        public int? C777 { get; set; }
        [Display(Name = "Monto de exoneración")]
        public int? C778 { get; set; }
        [Display(Name = "Subtotal de otras reducciones y exoneraciones")]
        public int? C779 { get; set; }
        [Display(Name = "Total gastos personales")]
        public int? C780 { get; set; }
        [Display(Name = "Valor impuesto pagado")]
        public int? C781 { get; set; }
        [Display(Name = "Valor impuesto pagado")]
        public int? C782 { get; set; }
        [Display(Name = "Ingresos")]
        public int? C783 { get; set; }
        [Display(Name = "Ingresos")]
        public int? C784 { get; set; }
        [Display(Name = "Ingresos")]
        public int? C786 { get; set; }
        [Display(Name = "Ingresos")]
        public int? C787 { get; set; }
        [Display(Name = "Ingresos")]
        public int? C789 { get; set; }
        [Display(Name = "Descripción")]
        public string C7004 { get; set; }
        [Display(Name = "Descripcion")]
        public string C7006 { get; set; }
        [Display(Name = "Valor")]
        public int? C7005 { get; set; }
        [Display(Name = "Valor")]
        public int? C7007 { get; set; }
        [Display(Name = "Base imponible gravada")]
        public int? C832 { get; set; }
        [Display(Name = "Total impusto causado")]
        public int? C839 { get; set; }
        [Display(Name = "Anticipo pagado")]
        public int? C840 { get; set; }
        [Display(Name = "Imp. a la renta causado")]
        public int? C842 { get; set; }
        [Display(Name = "Credito tributario por anticipos")]
        public int? C843 { get; set; }
        [Display(Name = "Retenciones")]
        public int? C845 { get; set; }
        [Display(Name = "Retenciones con dependencia")]
        public int? C846 { get; set; }
        [Display(Name = "Credito tributario por dividendos")]
        public int? C847 { get; set; }
        [Display(Name = "Retenciones por ingreso del exterior")]
        public int? C848 { get; set; }
        [Display(Name = "Anticipo de impuesto a la renta")]
        public int? C849 { get; set; }
        [Display(Name = "Credito tributario de años anteriores")]
        public int? C850 { get; set; }
        [Display(Name = "Credito tributario por salida de divisas")]
        public int? C851 { get; set; }
        [Display(Name = "Exoneraciony  credito tributario")]
        public int? C852 { get; set; }
        [Display(Name = "Subtotal impuesto a pagar")]
        public int? C855 { get; set; }
        [Display(Name = "Subtotal saldo a favor")]
        public int? C856 { get; set; }
        [Display(Name = "Impuesto a la renta unico")]
        public int? C857 { get; set; }
        [Display(Name = "Credito para liquidación Imp. renta")]
        public int? C858 { get; set; }
        [Display(Name = "Imp. a la renta a pagar")]
        public int? C859 { get; set; }
        [Display(Name = "Saldo a favor de contribuyente")]
        public int? C869 { get; set; }
        [Display(Name = "Anticipo")]
        public int? C880 { get; set; }
        [Display(Name = "Exoneraciones")]
        public int? C881 { get; set; }
        [Display(Name = "Otros conceptos")]
        public int? C882 { get; set; }
        [Display(Name = "Anticipo determinado próximo año")]
        public int? C879 { get; set; }
        [Display(Name = "Primera cuota")]
        public int? C871 { get; set; }
        [Display(Name = "Segunda cuota")]
        public int? C872 { get; set; }
        [Display(Name = "Pago previo")]
        public int? C890 { get; set; }
        [Display(Name = "Interes")]
        public int? C897 { get; set; }
        [Display(Name = "Impuesto")]
        public int? C898 { get; set; }
        [Display(Name = "Multa")]
        public int? C899 { get; set; }
        [Display(Name = "Total impuesto a pagar")]
        public int? C902 { get; set; }
        [Display(Name = "Interes por mora")]
        public int? C903 { get; set; }
        [Display(Name = "Multa")]
        public int? C904 { get; set; }
        [Display(Name = "Total Pagado")]
        public int? C999 { get; set; }
        [Display(Name = "Mediante cheque")]
        public int? C905 { get; set; }
        [Display(Name = "Mediante compensaciones")]
        public int? C906 { get; set; }
        [Display(Name = "Mediante notas de crédito")]
        public int? C907 { get; set; }
        [Display(Name = "N/C No")]
        public int? C908 { get; set; }
        [Display(Name = "USD")]
        public int? C909 { get; set; }
        [Display(Name = "N/C No")]
        public int? C910 { get; set; }
        [Display(Name = "USD")]
        public int? C911 { get; set; }
        [Display(Name = "N/C No")]
        public int? C912 { get; set; }
        [Display(Name = "USD")]
        public int? C913 { get; set; }
        [Display(Name = "USD")]
        public int? C915 { get; set; }
        [Display(Name = "Resol No.")]
        public int? C916 { get; set; }
        [Display(Name = "USD")]
        public int? C917 { get; set; }
        [Display(Name = "Resol No.")]
        public int? C918 { get; set; }
        [Display(Name = "USD")]
        public int? C919 { get; set; }
        [Display(Name = "USD")]
        public int? C920 { get; set; }
        [Display(Name = "Nombre")]
        public string Nombre { get; set; }
        [Display(Name = "Cédula de indentidad")]
        public int? C198 { get; set; }
    }
}
