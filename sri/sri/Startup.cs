using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using sri.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using sri.Models;

namespace sri
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseNpgsql(
                    Configuration.GetConnectionString("PostgresqlConnection")));
            services.AddDefaultIdentity<IdentityUser>(options => options.SignIn.RequireConfirmedAccount = false)
                .AddRoles<IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>();

            //

            services.AddAuthorization(options =>
            {
                //agrega politicas que establecen los roles
                options.AddPolicy("AdminAccess", policy => policy.RequireRole("Admin"));

                options.AddPolicy
                (
                    "ManagerAccess", policy =>
                    policy.RequireAssertion
                    (
                        context => context.User.IsInRole("Admin") || context.User.IsInRole("Manager")
                    )
                );

                options.AddPolicy
                (
                    "CustomerAccess",
                    policy =>
                    policy.RequireAssertion
                    (
                        context =>
                        context.User.IsInRole("Admin") || context.User.IsInRole("Manager") || context.User.IsInRole("Customer")
                    )
                );

                options.AddPolicy
                (
                    "DriverAccess",
                    policy =>
                    policy.RequireAssertion
                    (
                        context =>
                        context.User.IsInRole("Driver") || context.User.IsInRole("Manager") || context.User.IsInRole("Admin")
                    )

                );
                    }
                );

            //inyeccion dependencia tablas dev con contexto dev generado por scaffold
            var connection = Configuration.GetConnectionString("MssqlConnection");
            services.AddDbContext<declaracionesContext>(options => options.UseSqlServer(connection));

            services.AddControllersWithViews();
            services.AddRazorPages();


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });
        }
    }
}
