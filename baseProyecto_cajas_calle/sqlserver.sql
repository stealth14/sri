--
-- PostgreSQL database dump
--

-- Dumped from database version 10.10
-- Dumped by pg_dump version 10.10

-- Started on 2020-02-13 16:36:43

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2851 (class 0 OID 16483)
-- Dependencies: 198
-- Data for Name: AspNetRoleClaims; Type: TABLE DATA; Schema: public; Owner: ronny
--

COPY public."AspNetRoleClaims" ("Id", "RoleId", "ClaimType", "ClaimValue") FROM stdin;
\.


--
-- TOC entry 2850 (class 0 OID 16467)
-- Dependencies: 197
-- Data for Name: AspNetRoles; Type: TABLE DATA; Schema: public; Owner: ronny
--

COPY public."AspNetRoles" ("Id", "Name", "NormalizedName", "ConcurrencyStamp") FROM stdin;
db053b0b-d8bc-4ca1-b05e-8b7b4b7452d4	Admin	ADMIN	10265ae5-50f9-4959-bda6-f0d586be3c84
a737358c-858b-4d3a-96ae-544d7ce16d42	Customer	CUSTOMER	95fd880a-827e-48cc-b56c-ebb52289b5e5
96704268-de39-4ce9-b020-264193eadba7	Driver	DRIVER	33db22cb-2179-4491-a162-d9804c4ce400
\.


--
-- TOC entry 2852 (class 0 OID 16496)
-- Dependencies: 199
-- Data for Name: AspNetUserClaims; Type: TABLE DATA; Schema: public; Owner: ronny
--

COPY public."AspNetUserClaims" ("Id", "UserId", "ClaimType", "ClaimValue") FROM stdin;
\.


--
-- TOC entry 2853 (class 0 OID 16509)
-- Dependencies: 200
-- Data for Name: AspNetUserLogins; Type: TABLE DATA; Schema: public; Owner: ronny
--

COPY public."AspNetUserLogins" ("LoginProvider", "ProviderKey", "ProviderDisplayName", "UserId") FROM stdin;
\.


--
-- TOC entry 2854 (class 0 OID 16522)
-- Dependencies: 201
-- Data for Name: AspNetUserRoles; Type: TABLE DATA; Schema: public; Owner: ronny
--

COPY public."AspNetUserRoles" ("UserId", "RoleId") FROM stdin;
aba85f30-ac72-4a67-ba75-1508df07c5c4	db053b0b-d8bc-4ca1-b05e-8b7b4b7452d4
aba85f30-ac72-4a67-ba75-1508df07c5c4	a737358c-858b-4d3a-96ae-544d7ce16d42
aba85f30-ac72-4a67-ba75-1508df07c5c4	96704268-de39-4ce9-b020-264193eadba7
\.


--
-- TOC entry 2855 (class 0 OID 16540)
-- Dependencies: 202
-- Data for Name: AspNetUserTokens; Type: TABLE DATA; Schema: public; Owner: ronny
--

COPY public."AspNetUserTokens" ("UserId", "LoginProvider", "Name", "Value") FROM stdin;
\.


--
-- TOC entry 2856 (class 0 OID 16570)
-- Dependencies: 203
-- Data for Name: AspNetUsers; Type: TABLE DATA; Schema: public; Owner: ronny
--

COPY public."AspNetUsers" ("Id", "UserName", "NormalizedUserName", "Email", "NormalizedEmail", "EmailConfirmed", "PasswordHash", "SecurityStamp", "ConcurrencyStamp", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEnd", "LockoutEnabled", "AccessFailedCount") FROM stdin;
36a6e897-fb01-4ec2-b2d0-c22eb3be02c0	pato1418@yahoo.com	PATO1418@YAHOO.COM	pato1418@yahoo.com	PATO1418@YAHOO.COM	t	AQAAAAEAACcQAAAAEMtnJ3WyqjMZR1mLmXlzGA6Poyen32dnRGyQPqRj85J7mRs7gWJDK6kMFEgdQ5Y4SA==	HC6FGTMBOVIM3QNRZCDWQ6M2BQLN52UR	fcd17ef6-ec0a-4f7a-87f9-977bdd97dfce	\N	f	f	\N	t	0
400a05b6-7fb9-4393-bf10-00e639bc0616	kevin1418@yahoo.com	KEVIN1418@YAHOO.COM	kevin1418@yahoo.com	KEVIN1418@YAHOO.COM	f	AQAAAAEAACcQAAAAEFt0p1runWBa0ZT3arSsVXpovnhULmMzrr+7VW+iL+6AkMovhtK5oFAnzRTFzlqwYg==	6JLUXATTEJLI6AUUJSJHNP3YAO7K366H	c774f870-3dbd-4a5c-a54e-1fcc6f53712a	\N	f	f	\N	t	0
a0de75ee-093b-4e35-a81c-341d181ec3af	luis1418@yahoo.com	LUIS1418@YAHOO.COM	luis1418@yahoo.com	LUIS1418@YAHOO.COM	f	AQAAAAEAACcQAAAAEBAljSeuDGJpKHRkFMZ7cLIocmqamzYMatkkSib48FuerHzowBO16wjafOZbPNK95A==	4WYHFQ42DPSO47A6JRNJ77XQ64BD3NT5	612938d7-661a-4994-a95f-223fc0c9374a	\N	f	f	\N	t	0
aba85f30-ac72-4a67-ba75-1508df07c5c4	ronny1418@hotmail.com	RONNY1418@HOTMAIL.COM	ronny1418@hotmail.com	RONNY1418@HOTMAIL.COM	f	AQAAAAEAACcQAAAAEHPDB9PcdkFGlxs0laWMxMcSJ9db/6GaBN5CH6pQC0pV7jSWZoyptF8QPaQOC55s9Q==	F3JYPWGP6UTOHS3KBI2W2SXQ6MRPOOSG	c2326f87-258e-458f-a913-218526afc2e2	\N	f	f	\N	t	0
\.


--
-- TOC entry 2849 (class 0 OID 16462)
-- Dependencies: 196
-- Data for Name: __EFMigrationsHistory; Type: TABLE DATA; Schema: public; Owner: ronny
--

COPY public."__EFMigrationsHistory" ("MigrationId", "ProductVersion") FROM stdin;
00000000000000_CreateIdentitySchema	3.1.1
\.


-- Completed on 2020-02-13 16:36:43

--
-- PostgreSQL database dump complete
--

